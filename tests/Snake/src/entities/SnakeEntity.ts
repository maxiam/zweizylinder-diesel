import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import Vector2d from "../../../../src/models/Vector2d";
import SnakeGame, {Event} from "../SnakeGame";

export default class SnakeEntity extends GameEntity {
    readonly name = 'Snake';

    pieces: Vector2d[];
    moveDir: MoveDir;
    moveDirNext: MoveDir;
    dead: boolean;
    tickSpeed: number = 150;
    nextTick: number = 0

    init(): void {
        this.initSnake();
        this.events.on(Event.LvlUp, () => this.tickSpeed -= 10);
        this.events.on(Event.SnakeDirChanged, (dir) => this.moveDirNext = dir.data);
        this.events.on(Event.GameRestart, () => this.initSnake());
    }

    initSnake(): void {
        this.pieces = [
            {x: 6, y: 5},
            {x: 5, y: 5},
            {x: 4, y: 5},
        ];
        this.moveDir = MoveDir.Right;
        this.moveDirNext = null;
        this.dead = false;
        this.nextTick = 0;
        this.tickSpeed = 150;
    }

    update(elapsedTime: number): void {
        this.nextTick += elapsedTime;
        if (!this.dead) {
            this.moveSnake();
        }
    }

    moveSnake(): void {
        if (this.nextTick >= this.tickSpeed) {
            if (this.moveDir != MoveDir.Left && this.moveDirNext == MoveDir.Right) {
                this.moveDir = this.moveDirNext;
            }
            else if (this.moveDir != MoveDir.Right && this.moveDirNext == MoveDir.Left) {
                this.moveDir = this.moveDirNext;
            }
            else if (this.moveDir != MoveDir.Down && this.moveDirNext == MoveDir.Up) {
                this.moveDir = this.moveDirNext;
            }
            else if (this.moveDir != MoveDir.Up && this.moveDirNext == MoveDir.Down) {
                this.moveDir = this.moveDirNext;
            }

            const head = this.pieces[0];
            let pieceCopy = {x: head.x, y: head.y}
            switch (this.moveDir) {
                case MoveDir.Right:
                    head.x += 1;
                    break;
                case MoveDir.Left:
                    head.x -= 1;
                    break;
                case MoveDir.Up:
                    head.y -= 1;
                    break;
                case MoveDir.Down:
                    head.y += 1;
                    break;
            }

            if (head.x < 0) head.x = SnakeGame.BlockCount - 1;
            if (head.x >= SnakeGame.BlockCount) head.x = 0;
            if (head.y < 0) head.y = SnakeGame.BlockCount - 1;
            if (head.y >= SnakeGame.BlockCount) head.y = 0;

            for (let i = 1; i < this.pieces.length; i++) {
                const tmp = this.pieces[i];
                this.pieces[i] = pieceCopy;
                pieceCopy = {x: tmp.x, y: tmp.y}

                if (head.x === this.pieces[i].x && head.y === this.pieces[i].y) {
                    this.events.fire(Event.SnakeDied);
                    this.dead = true;
                }
            }
            this.events.fire(Event.SnakeMoved, this.pieces);
            this.nextTick = 0;
        }
    }

    draw(renderer: Renderer): void {
        for (let i = 0; i < this.pieces.length; i++) {
            renderer.drawPixel(
                Math.floor(this.pieces[i].x * SnakeGame.BlockSize + SnakeGame.MarginLeft),
                Math.floor(this.pieces[i].y * SnakeGame.BlockSize + SnakeGame.MarginTop),
                '#ffce53', SnakeGame.BlockSize, SnakeGame.BlockSize);
        }
    }
}

export enum MoveDir {
    Up = 'Up',
    Down = 'Down',
    Left = 'Left',
    Right = 'Right'
}
