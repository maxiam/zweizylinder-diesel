import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import Particle from "../../../../src/models/Particle";
import {randomFloatFromInterval, randomIntFromInterval} from "../../../../src/utils/RandomUtil";
import Vector2d from "../../../../src/models/Vector2d";
import SnakeGame from "../SnakeGame";

export default class StarsEntity extends GameEntity {
    readonly name = 'Stars';

    particleCount = 50;
    particles: Particle[] = [];

    init(): void {
        this.loadParticles();
    }

    private loadParticles() {
        for (let i = 0; i < this.particleCount; i++) {
            let x = randomIntFromInterval(0, SnakeGame.Canvas.width);
            let y = randomIntFromInterval(0, SnakeGame.Canvas.width);
            const size = randomIntFromInterval(1, 3);
            const speed = .05;

            const xSpeed = randomFloatFromInterval(-speed * size, speed * size);
            const ySpeed = randomFloatFromInterval(-speed * size, speed * size);

            this.particles.push(
                new Particle('#ffef96', size, new Vector2d(x, y), new Vector2d(xSpeed, ySpeed)));
        }
    }

    update(elapsedTime: number): void {
        const margin = 20;

        for (const p of this.particles) {
            p.position.x += p.speed.x * elapsedTime;
            p.position.y += p.speed.y * elapsedTime;

            if (p.position.x < -margin || p.position.x >= SnakeGame.Canvas.width + margin) {
                p.speed.x *= -1;
            }
            if (p.position.y < -margin || p.position.y >= SnakeGame.Canvas.height + margin) {
                p.speed.y *= -1;
            }
        }
    }

    draw(renderer: Renderer): void {
        for (const p of this.particles) {
            renderer.drawPixel(Math.floor(p.position.x), Math.floor(p.position.y), p.color, p.size, p.size);
        }
    }
}
