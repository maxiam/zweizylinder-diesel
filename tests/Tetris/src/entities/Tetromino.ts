import {Matrix2D} from "../Types";
import MatrixUtil from "../../../../src/utils/MatrixUtil";
import TetrisGame from "../TetrisGame";
import Playground from "./Playground";
import Renderer from "../../../../src/graphics/Renderer";

export default class Tetromino {
    static SpriteSheet: HTMLImageElement;

    pos: {x: number, y: number};
    playground: Playground;

    constructor(public matrix: Matrix2D<string>,
                public spriteIndex: number) {
    }

    rotateLeft() {
        const matrix = this.matrix;
        MatrixUtil.rotateMatrix90DegCounterClock(matrix);

        if (this.testCollision()) {
            MatrixUtil.rotateMatrix90DegCounterClock(matrix);
            MatrixUtil.rotateMatrix90DegCounterClock(matrix);
            MatrixUtil.rotateMatrix90DegCounterClock(matrix);
        }
    }

    rotateRight() {
        this.rotateLeft();
        this.rotateLeft();
        this.rotateLeft();

        if (this.testCollision()) {
            this.rotateLeft();
        }
    }

    moveRight() {
        this.pos.x++;
        if (this.testCollision()) {
            this.pos.x--;
        }
    }

    moveLeft() {
        this.pos.x--;
        if (this.testCollision()) {
            this.pos.x++;
        }
    }

    moveDown(): boolean {
        this.pos.y++;
        if (this.testCollision()) {
            this.pos.y--;
            return false;
        }

        return true;
    }

    softDrop() {
        this.pos.y++;
        if (this.testCollision()) {
            this.pos.y--;
        }
    }

    hardDrop() {
        let dropped = false;
        do {
            dropped = this.moveDown();
        } while (dropped);
    }

    testCollision(): boolean {
        const matrix = this.matrix;
        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < matrix[y].length; x++) {
                const brick = matrix[y][x];
                const xPos = x + this.pos.x;
                const yPos = y + this.pos.y;
                if (brick !== ' ') {
                    if (yPos >= TetrisGame.FieldHeight) {
                        return true;
                    }
                    if (xPos >= TetrisGame.FieldWidth) {
                        return true;
                    }
                    if (xPos < 0) {
                        return true;
                    }
                    if (this.playground.matrix[yPos][xPos]) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    draw(renderer: Renderer, shadow = false) {
        const blockSize = TetrisGame.BlockSize;
        let matrix = this.matrix;
        let pos = this.pos;
        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < matrix[y].length; x++) {
                const block: string = matrix[y][x];
                if (block !== ' ') {
                    if (!shadow) {
                        renderer.drawImage(Tetromino.SpriteSheet,
                            0,SpriteIndex[block as keyof typeof SpriteIndex] * TetrisGame.BlockSize,TetrisGame.BlockSize,TetrisGame.BlockSize,
                            x * blockSize + pos.x * blockSize,
                            (y - TetrisGame.VisibleAt) * blockSize + pos.y * blockSize,
                            TetrisGame.BlockSize, TetrisGame.BlockSize);
                    }
                    else {
                        renderer.drawPixel(
                            x * blockSize + pos.x * blockSize,
                            (y - TetrisGame.VisibleAt) * blockSize + pos.y * blockSize,
                            'rgba(133,86,110,0.66)',
                            TetrisGame.BlockSize, TetrisGame.BlockSize
                            )
                    }
                }
            }
        }
    }
}

export enum SpriteIndex {
    Z,
    O,
    I,
    L,
    J,
    T,
    S,
}
