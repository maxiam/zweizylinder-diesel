import Renderer from "../graphics/Renderer";
import EventManager from "./events/EventManager";
import GameEntityManager from "./GameEntityManager";

export default abstract class GameEntity {
    abstract readonly name: string;
    private eventManager: EventManager;
    private entityManager: GameEntityManager;

    public setEventManager(eventManager: EventManager): void {
        this.eventManager = eventManager;
    }

    public setEntityManager(entityManager: GameEntityManager): void {
        this.entityManager = entityManager;
    }

    protected get events(): EventManager {
        return this.eventManager;
    }

    protected getEntity(name: string): GameEntity {
        return this.entityManager.getEntityByName(name);
    }

    abstract init(): void;
    abstract update(elapsedTime: number): void;
    abstract draw(renderer: Renderer): void;

}
