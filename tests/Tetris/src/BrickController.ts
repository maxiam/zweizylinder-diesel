import ControlsFactory, {TetrisControls} from "./factories/ControlsFactory";
import Playground from "./entities/Playground";
import InputManager from "../../../src/inputs/InputManager";

export default class BrickController {

    private keyboardControls = ControlsFactory.createKeyboardControls();
    private gamepadControls = ControlsFactory.createGamepadControls();

    private rowsPerLeveL = 5;
    private dropIntervals = [1000, 750, 500, 250, 50];
    private currInterval = this.dropIntervals.shift();
    private timeToDrop = 0;

    constructor(private playground: Playground,
                private inputManager: InputManager) {

    }

    update(elapsedTime: number): boolean {
        this.timeToDrop += elapsedTime;

        this.handlePlayerInput();

        if (this.timeToDrop > this.currInterval) {
            this.timeToDrop = 0;
            if (!this.moveDown()) {
                this.playground.placeBrick();
            }
        }

        this.playground.updateRows();
        this.playground.updateShadowBrick();

        if (this.playground.levelClearedRows >= this.rowsPerLeveL) {
            this.currInterval = this.dropIntervals.shift();
            this.playground.levelClearedRows = 0;
            this.playground.lvl++;
        }

        if (this.playground.tCurr === null) {
            this.playground.addTetromino();
            if (this.playground.tCurr.testCollision()) {
                return false;
            }
        }

        return true;
    }

    private handlePlayerInput() {
        const kb = this.inputManager.getKeyboard();
        const gamepad = this.inputManager.getPrimaryGamepad();

        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.MoveRight)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.MoveRight))) {
            this.moveRight();
        }
        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.MoveLeft)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.MoveLeft))) {
            this.moveLeft();
        }

        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.RotateRight)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.RotateRight))) {
            this.rotateRight();
        }

        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.RotateLeft)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.RotateLeft))) {
            this.rotateLeft();
        }

        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.SoftDrop)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.SoftDrop))) {
            this.softDrop();
        }

        if (kb.isKeyDown(this.keyboardControls.get(TetrisControls.HardDrop)) ||
            gamepad?.isButtonDown(this.gamepadControls.get(TetrisControls.HardDrop))) {
            this.hardDrop();
            this.playground.placeBrick();
            this.timeToDrop = this.currInterval;
        }
    }

    private rotateLeft() {
        this.playground.tCurr.rotateLeft();
    }

    private rotateRight() {
        this.playground.tCurr.rotateRight();
    }

    private moveRight() {
        this.playground.tCurr.moveRight();

    }

    private moveLeft() {
        this.playground.tCurr.moveLeft();

    }

    private moveDown(): boolean {
        return this.playground.tCurr.moveDown();

    }

    private softDrop() {
        this.playground.tCurr.softDrop();

    }

    private hardDrop() {
        this.playground.tCurr.hardDrop();
    }
}
