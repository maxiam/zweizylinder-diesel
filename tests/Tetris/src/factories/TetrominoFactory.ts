import Tetromino, {SpriteIndex} from "../entities/Tetromino";

export default class TetrominoFactory {

    static get I() {
        return new Tetromino([
            [' ', ' ', ' ', ' '],
            ['I', 'I', 'I', 'I'],
            [' ', ' ', ' ', ' '],
            [' ', ' ', ' ', ' ']
        ], SpriteIndex.I);
    }

    static get J() {
        return new Tetromino([
            ['J', ' ', ' '],
            ['J', 'J', 'J'],
            [' ', ' ', ' '],
        ], SpriteIndex.J);
    }

    static get L() {
        return new Tetromino([
            [' ', ' ', 'L'],
            ['L', 'L', 'L'],
            [' ', ' ', ' '],
        ], SpriteIndex.L);
    };

    static get O() {
        return new Tetromino([
            ['O', 'O'],
            ['O', 'O'],
        ], SpriteIndex.O);
    }

    static get S() {
        return new Tetromino([
            [' ', 'S', 'S'],
            ['S', 'S', ' '],
            [' ', ' ', ' '],
        ], SpriteIndex.S);
    }

    static get T() {
        return new Tetromino([
            [' ', 'T', ' '],
            ['T', 'T', 'T'],
            [' ', ' ', ' '],
        ], SpriteIndex.T);
    }

    static get Z() {
        return new Tetromino([
            ['Z', 'Z', ' '],
            [' ', 'Z', 'Z'],
            [' ', ' ', ' '],
        ], SpriteIndex.Z);
    }

    static get Bricks() {
        return [TetrominoFactory.I, TetrominoFactory.J, TetrominoFactory.L, TetrominoFactory.O, TetrominoFactory.S, TetrominoFactory.T, TetrominoFactory.Z];
    }
}

