import Vector2d from "./Vector2d";

export default class Particle {
    constructor(public color: string, public size: number, public position: Vector2d, public speed: Vector2d) {
    }
}
