import WebGameEngine from "../../../src/WebGameEngine";
import InputManager from "../../../src/inputs/InputManager";
import GameEntityManager from "../../../src/entities/GameEntityManager";
import SnakeEntity from "./entities/SnakeEntity";
import FruitEntity from "./entities/FruitEntity";
import StarsEntity from "./entities/StarsEntity";
import ScoreEntity from "./entities/ScoreEntity";
import ArenaEntity from "./entities/ArenaEntity";
import FpsEntity from "./entities/FpsEntity";
import ControlsEntity from "./entities/ControlsEntity";

export default class SnakeGame extends WebGameEngine {

    public static readonly BlockSize: number = 10;
    public static readonly BlockCount: number = 30;

    public static get MarginLeft() {
        return (SnakeGame.Canvas.width - (SnakeGame.BlockCount * SnakeGame.BlockSize)) / 2;
    }

    public static get MarginTop() {
        return (SnakeGame.Canvas.height - (SnakeGame.BlockCount * SnakeGame.BlockSize)) / 2;
    }

    inputManager: InputManager;
    entityManager: GameEntityManager;

    onLoad(): void {
        this.inputManager = new InputManager();
        this.inputManager.loadKeyboard();

        this.entityManager = new GameEntityManager();
        this.entityManager.addList([
            new ControlsEntity(this.inputManager),
            new ArenaEntity(),
            new SnakeEntity(),
            new FruitEntity(),
            new StarsEntity(),
            new ScoreEntity(),
            new FpsEntity()
        ]);
        this.startGame();
    }

    startGame() {
        this.entityManager.init();
    }

    onUpdate(elapsedTime: number): void {
        this.renderer.clearScreen();
        this.entityManager.update(elapsedTime);
        this.entityManager.draw(this.renderer);
    }
}

export enum Event {
    SnakeMoved = 'SnakeMoved',
    FruitEaten = 'FruitEaten',
    SnakeDied = 'SnakeDied',
    LvlUp = 'LvlUp',
    SnakeDirChanged = 'SnakeDirChanged',
    GameRestart = 'GameRestart',
}
