const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: 'development',
    entry: {
        Tetris: './tests/Tetris/index.ts',
        Snake: './tests/Snake/index.ts',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: '[name]/game.js',
        path: path.resolve(__dirname, 'dist'),
        clean: true
    },
    devServer: {
        static: {
            directory: path.resolve(__dirname, 'dist'),
        },
        compress: true,
        port: 9000,
        devMiddleware: {
            writeToDisk: true
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Tetris',
            template: './tests/Tetris/index.html',
            filename: './Tetris/index.html',
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Snake',
            template: './tests/Snake/index.html',
            filename: './Snake/index.html',
            inject: false
        }),
        new CopyPlugin({
            patterns: [
                {from: "./tests/Tetris/images", to: "./Tetris/images"},
            ],
        }),
    ]
};
