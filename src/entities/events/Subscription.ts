import GameEvent from "./GameEvent";
import EventManager from "./EventManager";

export default class Subscription {

    constructor(private event: string,
                private callback: (evt: GameEvent) => void,
                private manager: EventManager) {
    }

    unsubscribe(): void {
        this.manager.unsubscribe(this.event, this.callback);
    }
}
