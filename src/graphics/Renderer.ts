export default interface Renderer {

    clearScreen(): void;

    drawPixel(x: number, y: number, color: string, width: number, height: number): void;

    drawLine(xStart: number, yStart: number, xEnd: number, yEnd: number, width: number, color: string): void;

    drawImage(img: CanvasImageSource, x: number, y: number, w: number, h: number,
              dx: number, dy: number, dw: number, dh: number): void;

    drawText(text: string | string[], x: number, y: number, color: string, font: string, size: number): void;

    finalize(): void;
}
