import TetrisGame from "./src/TetrisGame";

const tetris = new TetrisGame();
tetris.createCanvas2D(document.getElementById('game'), 600, 600);
tetris.start();
