import GameEntity from "./GameEntity";
import Renderer from "../graphics/Renderer";
import EventManager from "./events/EventManager";

export default class GameEntityManager {

    private entities: GameEntity[] = [];
    private entitiesByName: Map <string, GameEntity> = new Map();
    private eventManager = new EventManager();

    add(entity: GameEntity): void {
        this.entities.push(entity);
        this.entitiesByName.set(entity.name, entity);
        entity.setEventManager(this.eventManager);
        entity.setEntityManager(this);
    }

    addList(entities: GameEntity[]) {
        for (let entity of entities) {
            this.add(entity);
        }
    }

    remove(entity: GameEntity): void {
        delete this.entities[this.entities.indexOf(entity)];
        this.entitiesByName.delete(entity.name);
    }

    getEntityByName<T extends GameEntity>(name: string): GameEntity {
        return this.entitiesByName.get(name);
    }

    init(): void {
        for (let i = 0; i < this.entities.length; i++) {
            this.entities[i].init();
        }
    }

    update(elapsedGameTime: number): void {
        for (let i = 0; i < this.entities.length; i++) {
            this.entities[i].update(elapsedGameTime);
        }
    }

    draw(renderer: Renderer): void {
        for (let i = 0; i < this.entities.length; i++) {
            this.entities[i].draw(renderer);
        }
    }
}
