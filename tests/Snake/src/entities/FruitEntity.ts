import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import Vector2d from "../../../../src/models/Vector2d";
import {randomIntFromInterval} from "../../../../src/utils/RandomUtil";
import SnakeGame, {Event} from "../SnakeGame";

export default class FruitEntity extends GameEntity {
    readonly name = 'Fruit';

    position: Vector2d;

    init(): void {
        this.spawnFruit();
        this.events.on(Event.SnakeMoved, (evt) =>
            this.handleSnakeMoved(evt.data as Vector2d[]));
    }

    private handleSnakeMoved(snakePieces: Vector2d[]) {
        const head = snakePieces[0];
        if (head.x === this.position.x && head.y === this.position.y) {
            snakePieces.push(this.position);
            this.events.fire(Event.FruitEaten);
            this.spawnFruit(snakePieces);
        }
    }

    private spawnFruit(snakePieces: Vector2d[] = []) {
        do {
            this.position = {
                x: randomIntFromInterval(0, SnakeGame.BlockCount - 1),
                y: randomIntFromInterval(0, SnakeGame.BlockCount - 1)
            }
        } while (snakePieces.find(p => p.x === this.position.x && p.y === this.position.y))
    }

    update(elapsedGameTime: number): void {
    }

    draw(renderer: Renderer): void {
        renderer.drawPixel(
            Math.floor(this.position.x * SnakeGame.BlockSize + SnakeGame.MarginLeft),
            Math.floor(this.position.y * SnakeGame.BlockSize + SnakeGame.MarginTop),
            '#ff4949', SnakeGame.BlockSize, SnakeGame.BlockSize)
    }
}
