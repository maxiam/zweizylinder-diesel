import {Matrix2D} from "../Types";
import Renderer from "../../../../src/graphics/Renderer";
import TetrisGame from "../TetrisGame";
import Tetromino, {SpriteIndex} from "./Tetromino";
import TetrominoFactory from "../factories/TetrominoFactory";
import ArrayUtil from "../../../../src/utils/ArrayUtil";

export default class Playground {
    public matrix: Matrix2D<string>;

    public tCurr: Tetromino = null;
    public tShadow: Tetromino = null;
    public tNext: Tetromino[] = [];

    public lvl = 1;
    public levelClearedRows = 0;
    public totalClearedRows = 0;

    constructor() {
        this.matrix = new Array(TetrisGame.FieldHeight);
        for (let i = 0; i < this.matrix.length; i++) {
            this.matrix[i] = new Array(TetrisGame.FieldWidth);
        }

        this.loadBucket();
        this.addTetromino();
    }


    public addTetromino() {
        const curr = this.tNext.shift();
        curr.playground = this;
        const shadow = new Tetromino(curr.matrix, curr.spriteIndex);
        shadow.playground = this;
        curr.pos = {
            x: Math.floor(this.matrix[0].length / 2 - curr.matrix[0].length / 2),
            y: 19
        };
        shadow.pos = {
            x: curr.pos.x,
            y: curr.pos.y
        };
        this.tCurr = curr;
        this.tShadow = shadow;
    }

    public updateShadowBrick() {
        if (this.tCurr) {
            const sb = this.tShadow;
            sb.pos.y = 0;
            sb.pos.x = this.tCurr.pos.x;
            sb.hardDrop();
        }
    }

    placeBrick() {
        const matrix = this.tCurr.matrix;
        const pos = this.tCurr.pos;
        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < matrix[y].length; x++) {
                let brick = matrix[y][x];
                if (brick !== ' ') {
                    this.matrix[y + pos.y][x + pos.x] = brick;
                }
            }
        }
        this.tCurr = null;
        this.fillBucket();
    }

    private loadBucket() {
        this.tNext = TetrominoFactory.Bricks;
        ArrayUtil.shuffle(this.tNext);
    }

    private fillBucket() {
        const bricks = TetrominoFactory.Bricks;
        ArrayUtil.shuffle(bricks);
        this.tNext.push(bricks[0])
    }

    public updateRows() {
        const fullRows = this.getFullRows();
        if (fullRows.length) {
            this.clearRows(fullRows);
            this.fillEmptyRows(fullRows);
            this.levelClearedRows += fullRows.length;
            this.totalClearedRows += fullRows.length;
        }
    }
    
    private getFullRows(): number[] {
        const rows = [];
        for (let y = 0; y < this.matrix.length; y++) {
            let fullRow = true;
            for (let x = 0; x < this.matrix[y].length; x++) {
                if (!this.matrix[y][x]) {
                    fullRow = false;
                }
            }
            if (fullRow) {
                rows.push(y);
            }

        }
        return rows;
    }

    private clearRows(fullRows: number[]) {
        for (const row of fullRows) {
            for (let i = 0; i < this.matrix[row].length; i++) {
                this.matrix[row][i] = null;
            }
        }
    }

    private fillEmptyRows(fullRows: number[]) {
        for (let i = 0; i < fullRows.length; i++) {
            const rowIdx = fullRows[i];
            for (let y = rowIdx; y > 0; y--) {
                const currRow = this.matrix[y];
                this.matrix[y] = this.matrix[y - 1];
                this.matrix[y - 1] = currRow;
            }
        }
    }

    draw(renderer: Renderer) {
        const blockSize = TetrisGame.BlockSize;

        for (let x = 0; x <= TetrisGame.FieldWidth; x++) {
            renderer.drawLine(x * blockSize, 0, x * blockSize, TetrisGame.FieldHeight * blockSize - TetrisGame.VisibleAt * blockSize, 1, '#525252');
        }

        for (let y = 0; y <= TetrisGame.FieldHeight - TetrisGame.VisibleAt; y++) {
            renderer.drawLine(0, y * blockSize, TetrisGame.FieldWidth * blockSize, y * blockSize, 1, '#525252');
        }

        for (let x = 0; x < TetrisGame.FieldWidth; x++) {
            for (let y = 0; y < TetrisGame.FieldHeight; y++) {
                this.drawBlock(x, y, renderer);
            }
        }
        if (this.tCurr) {
            this.tShadow.draw(renderer, true);
            this.tCurr.draw(renderer);
        }

        const xOffset = 330;
        const yOffset = 530;
        const size = 30;
        renderer.drawText(`Level:  ${this.lvl}`, xOffset, size + yOffset, 'white', 'monospace', size);
        renderer.drawText(`Points: ${this.totalClearedRows}`, xOffset, size * 2 + yOffset, 'white', 'monospace', size);

        for (let i = 0; i < 3; i++) {
            const nextBrick = this.tNext[i];
            nextBrick.pos = {x: 11, y: 20 + i * 3};
            nextBrick.draw(renderer);
        }
    }

    drawBlock(x: number, y: number, renderer: Renderer): void {
        const element = this.matrix[y][x];
        if (element === null) return;
        const blockSize = TetrisGame.BlockSize;

        renderer.drawImage(
            Tetromino.SpriteSheet,
            0, SpriteIndex[element as keyof typeof SpriteIndex] * blockSize, blockSize, blockSize,
            x * blockSize,
            (y - TetrisGame.VisibleAt) * blockSize,
            blockSize,
            blockSize);
    }
}
