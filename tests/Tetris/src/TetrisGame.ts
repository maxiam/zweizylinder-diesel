import WebGameEngine from '../../../src/WebGameEngine';
import BrickController from "./BrickController";
import Playground from "./entities/Playground";
import ImageLoader from "../../../src/io/ImageLoader";
import InputManager from "../../../src/inputs/InputManager";
import Tetromino from "./entities/Tetromino";

export default class TetrisGame extends WebGameEngine {

    public static readonly FieldWidth = 10;
    public static readonly FieldHeight = 40;
    public static readonly VisibleAt = 20;
    public static readonly BlockSize = 30;

    inputManager: InputManager;
    playground: Playground;
    brickController: BrickController;

    async onLoad() {
        Tetromino.SpriteSheet = await ImageLoader.load('images/bricks.png');

        this.inputManager = new InputManager()
        this.inputManager.loadKeyboard();
        this.inputManager.loadGamepad();

        this.startGame();
    }

    startGame() {
        this.playground = new Playground();
        this.brickController = new BrickController(this.playground, this.inputManager);
    }

    onUpdate(elapsedTime: number) {
        this.renderer.clearScreen();
        this.inputManager.update();

        const gameRunning = this.brickController.update(elapsedTime);

        this.playground.draw(this.renderer);
        this.inputManager.getKeyboard().reset();

        if (!gameRunning) {
            this.startGame();
        }
    }
}
