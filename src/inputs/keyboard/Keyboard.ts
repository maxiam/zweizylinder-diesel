export default class Keyboard {

    private loaded = false;
    private readonly downKeys = new Map<string, true>();
    private readonly pressedKeys = new Map<string, true>();

    load() {
        if (this.loaded) {
            throw new Error('Keyboard already loaded');
        }
        window.addEventListener('keydown', this.keydownEvent);
        window.addEventListener('keyup', this.keyupEvent);
        this.loaded = true;
    }

    private keydownEvent = (event: KeyboardEvent) => {
        this.downKeys.set(event.key, true);
        this.pressedKeys.set(event.key, true);
    }

    private keyupEvent = (event: KeyboardEvent) => {
        this.pressedKeys.delete(event.key);
    }

    isKeyDown(key: string): boolean {
        return this.downKeys.has(key);
    }

    isKeyPressed(key: string): boolean {
        return this.pressedKeys.has(key);
    }

    reset() {
        this.downKeys.clear();
    }

    printKeys() {
        console.log(this.pressedKeys);
    }
}
