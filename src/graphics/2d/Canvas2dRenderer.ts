import Renderer from "../Renderer";
import CanvasFactory from "../CanvasFactory";

export default class Canvas2dRenderer implements Renderer {

    private readonly context: CanvasRenderingContext2D;
    private readonly buffer: CanvasRenderingContext2D;

    constructor(canvas: HTMLCanvasElement) {
        this.context = canvas.getContext('2d');
        this.buffer = this.createBufferCanvas().getContext('2d');
    }

    protected createBufferCanvas(): HTMLCanvasElement {
        return CanvasFactory.createCanvas(this.context.canvas.width, this.context.canvas.height)
    }

    clearScreen(): void {
        this.buffer.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    }

    drawPixel(x: number, y: number, color: string, width = 1, height = 1): void {
        this.buffer.fillStyle = color;
        this.buffer.fillRect(Math.floor(x), Math.floor(y), Math.floor(width), Math.floor(height));
    }

    drawLine(xStart: number, yStart: number, xEnd: number, yEnd: number, width: number, color: string): void {
        this.buffer.beginPath();
        this.buffer.lineWidth = width;
        this.buffer.strokeStyle = color;
        this.buffer.moveTo(xStart, yStart);
        this.buffer.lineTo(xEnd, yEnd);
        this.buffer.stroke();
    }

    drawImage(image: CanvasImageSource,
              x: number, y: number, w: number, h: number,
              dx: number, dy: number, dw: number, dh: number): void {
        this.buffer.drawImage(image, x, y, w, h, dx, dy, dw, dh);
    }

    drawText(text: string | string[], x: number, y: number, color: string, font: string, size: number) {
        this.buffer.fillStyle = color;
        this.buffer.font = `${size}px ${font}`;
        if (Array.isArray(text)) {
            for (let i = 0; i < text.length; i++) {
                this.buffer.fillText(text[i], x, y + size * i);
            }
        } else {
            this.buffer.fillText(text, x, y);
        }
    }


    finalize(): void {
        this.context.drawImage(this.buffer.canvas, 0, 0);
    }
}
