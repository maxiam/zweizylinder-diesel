import SnakeGame from "./src/SnakeGame";


const tetris = new SnakeGame();
tetris.createCanvas2D(document.getElementById('game'), window.innerWidth, window.innerHeight);
tetris.start();
