import Renderer from "./graphics/Renderer";
import Canvas2dRenderer from "./graphics/2d/Canvas2dRenderer";
import CanvasFactory from "./graphics/CanvasFactory";

export default abstract class WebGameEngine {

    protected renderer: Renderer;
    public static Canvas: HTMLCanvasElement;

    public createCanvas2D(target: HTMLElement, canvasWidth: number, canvasHeight: number): void {
        const canvas = CanvasFactory.createCanvas(canvasWidth, canvasHeight);
        this.renderer = new Canvas2dRenderer(canvas);
        target.appendChild(canvas);
        WebGameEngine.Canvas = canvas;
    }

    public async start() {
        await this.onLoad();
        await this.startGameLoop();
    }

    protected startGameLoop() {
        this.timeLastRound = new Date().getTime();
        this.update();
    }

    timeThisRound = 0;
    deltaTime = 0;
    timeLastRound = 0;
    protected update(): void {
        this.timeThisRound = new Date().getTime();
        this.deltaTime = this.timeThisRound - this.timeLastRound;
        this.onUpdate(this.deltaTime);
        this.renderer.finalize();
        this.timeLastRound = this.timeThisRound;
        requestAnimationFrame(() => this.update());
    }

    public abstract onLoad(): void;
    public abstract onUpdate(elapsedTime: number): void;
}
